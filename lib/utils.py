from typing import List, Tuple


# Fonction pour décaler les valeurs d'une liste
def shift(list: List) -> List:
    return list.append(list.pop(0))


# Une fonction pour la distance entre deux points
def man_distance(i: Tuple, j: Tuple) -> int:
    return abs(i[0] - j[0]) + abs(i[1] - j[1])


# Une fonction pour les voisins d'une case
def neighbors(pos: Tuple, world_size: int) -> List:
    # Génération des cases qui sont autour de la case considérée
    neighbors = []
    if pos[0] > 0:
        neighbors.append((pos[0]-1, pos[1]))
    if pos[0] < world_size-1:
        neighbors.append((pos[0]+1, pos[1]))
    if pos[1] > 0:
        neighbors.append((pos[0], pos[1]-1))
    if pos[1] < world_size-1:
        neighbors.append((pos[0], pos[1]+1))
    return neighbors
