from typing import Tuple, List
from lib.utils import man_distance


class Node():
    def __init__(self, pos: Tuple, parent=None):
        self.pos = pos
        self.parent = parent

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.pos == other.pos


def astar(pos_ok: List, start_pos: Tuple, target_pos: Tuple) -> List:
    # Création des nœuds de départ et de but
    start_node = Node(start_pos)
    target_node = Node(target_pos)

    # Listes des positions qu'on peut explorer et de celles qu'il faut laisser
    # tomber
    to_explore = [start_node]
    already_explored = []

    # Tant qu'il reste des cases à vérifier
    while len(to_explore) != 0:
        # On cherche le nœud qui a le f le plus bas
        current_node = to_explore[0]
        for node in to_explore:
            if node.f < current_node.f:
                current_node = node

        # Il faut tester si c'est la cible (si oui, on peut arrêter)
        if current_node == target_node:
            # Reconstruisons le chemin
            path = []
            while current_node.parent is not None:
                path.append(current_node.pos)
                current_node = current_node.parent
            return path

        # Sinon, le nœud sera considéré comme vérifié
        to_explore.remove(current_node)
        already_explored.append(current_node)

        # On génère ses enfants (les cases adjacentes qui sont ok aussi)
        children = []
        i, j = current_node.pos
        if (i-1, j) in pos_ok:
            children.append(Node((i-1, j), current_node))
        if (i, j-1) in pos_ok:
            children.append(Node((i, j-1), current_node))
        if (i+1, j) in pos_ok:
            children.append(Node((i+1, j), current_node))
        if (i, j+1) in pos_ok:
            children.append(Node((i, j+1), current_node))

        # On va explorer chaque enfant
        for child in children:
            # Si l'enfant a déjà été passé en revue, c'est pas la peine
            if child in already_explored:
                continue

            # Calcul des valeurs g, h et f
            child.g = current_node.g + 1
            # Carré de la distance
            child.h = man_distance(child.pos, target_pos)
            child.f = child.g + child.h

            # Si l'enfant est déjà dans la liste avec un plus petit g, on ne le
            # rajoute pas (il a déjà été atteint dans l'exploration)
            if child not in to_explore:
                to_explore.append(child)
            else:
                index = to_explore.index(child)
                if to_explore[index].g > child.g:
                    to_explore.append(child)

    # Si on est rendu là, c'est qu'aucun chemin n'a été trouvé
    return None
