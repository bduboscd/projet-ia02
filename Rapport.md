# Comment lancer le programme
Il y a une dépendance à installer avec pip : `progressbar`.

Il faut ensuite placer l'exécutable gophersat, sans extension ni numéro de version, dans le dossier lib. Il suffit alors d'exécuter la commande "python main.py" et roulez jeunesse !

On peut aussi passer les arguments "-v" pour que le programme explique chacune de ses étapes, et "-d" pour donner les détails des appels au serveur.

# Ce que fait notre programme
## Interfaçage avec le client Wumpus
Grâce aux exemples, nous avons pu intégrer à notre programme la connexion au serveur, les commandes pour déclarer la fin de chacune des phases de jeu et celles pour indiquer qu'une fosse ou le Wumpus ont été débusqués.

## Phase 1
Pour construire la carte avec l'action de sondage, il faut savoir si la case qu'on essaie de sonder est sûre (auquel cas on envoie une sonde qui coûte 10 PO) ou non (et on envoie une sonde plus sophistiquée qui coûte 50 PO).

Pour ce faire, on tient donc trois listes : celle des cases sûres, celle des cases dangereuses, et celles pour lesquelles on ne sait pas.

Ces listes sont remplies grâce à ce qu'on déduit avec le solveur, et pas directement avec les percepts ("." ou "W" par exemple).

Tant que toutes les cases n'ont pas été explorées, on essaie de déduire la dangerosité des cases encore inconnues, et on envoie une sonde si on y arrive. Pour éviter au maximum les appels infructueux au solveur (quand il n'y a pas assez d'information), on l'appelle en priorité sur les cases voisines d'une case déjà explorée.

## Phase 2
Toutes les cases sur lesquelles on a trouvé de l'or sont rangées une première fois avec un algorithme glouton (dans l'ordre qui fait que le trajet est le plus petit entre chaque étape). Elles sont ensuite réordonnées en testant des permutations de ces étapes qui diminuent le coût total du trajet. Au maximum 100 permutations sont effectuées, le programme s'arrête forcément.

On appelle ensuite une fonction de pathfinding qui implémente A*. Grâce à la fonction go_to de la librairie Wumpus, le joueur visite chacune des cases du chemin calculé grâce à cet algorithme.

Le chemin retour vers la case de départ est aussi calculé pour pouvoir déclarer la phase 2 comme finie.
